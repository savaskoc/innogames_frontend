# Web UI for Horse Race Simulator

This is a react app bundled with parcel.

## Environment Variables

|Name   |Default                    |Description
|-------|---------------------------|-----------
|API_URL|http://127.0.0.1:8000      |Backend Url

You can see these variables in .env file.

## Docker Compose

Also you can use [docker-compose](https://bitbucket.org/savaskoc/innogames_docker) repository for quick testing.
