import React, {Fragment, useEffect, useState} from 'react'

import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import Table from 'react-bootstrap/Table'

export default function BestHorse({api}) {
    const [best, setBest] = useState([])
    const fetchBest = async function () {
        const {data: best} = await api.get('stats/best')
        setBest(best)
    }
    const syncFetchBest = function () {
        fetchBest()
    }

    useEffect(syncFetchBest, [])

    const renderBest = function () {
        const {id, speed, strength, endurance, distance, ticks} = best
        return <tr>
            <td>{id}</td>
            <td>{speed}</td>
            <td>{strength}</td>
            <td>{endurance}</td>
            <td>{distance}</td>
            <td>{ticks}</td>
        </tr>
    }

    return <Fragment>
        <Row>
            <Col md="auto"><h3>Best Horse</h3></Col>
            <Col>
                <Button variant="primary" onClick={syncFetchBest}><i className="fa fa-refresh mr-1"/>Refresh</Button>
            </Col>
        </Row>
        <Table striped bordered>
            <thead>
            <tr>
                <th>#</th>
                <th>Speed</th>
                <th>Strength</th>
                <th>Endurance</th>
                <th>Distance</th>
                <th>Time</th>
            </tr>
            </thead>
            <tbody>
            {best && renderBest()}
            </tbody>
        </Table>
    </Fragment>
}
