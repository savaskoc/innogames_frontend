import React, {Fragment, useEffect, useState} from 'react'
import classNames from 'classnames'

import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import Alert from 'react-bootstrap/Alert'
import Table from 'react-bootstrap/Table'

export default function RaceDetail({api, raceId}) {
    const [error, setError] = useState(null)
    const [detail, setDetail] = useState(null)

    const loadDetail = async function () {
        setError(null)
        setDetail(null)
        try {
            const {data} = await api.get(`races/${raceId}`)
            setDetail(data)
        } catch (e) {
            setError(e.message)
        }
    }
    const advance = async function () {
        setError(null)
        try {
            const {data} = await api.post(`races/${raceId}/advance`)
            setDetail(data)
        } catch (e) {
            setError(e.message)
        }
    }

    useEffect(() => {
        loadDetail()
    }, [raceId])

    const renderHorse = function ({id, speed, strength, endurance, distance, ticks}, i) {
        return <tr key={i}>
            <td>{id}</td>
            <td>{speed}</td>
            <td>{strength}</td>
            <td>{endurance}</td>
            <td>{distance}</td>
            <td>{ticks}</td>
        </tr>
    }

    const renderDetail = function () {
        const {id, createdAt, isCompleted, horses} = detail
        return <Fragment>
            <strong>Race #{id}</strong>
            <div>
                <strong>Created At:</strong>
                <span className="ml-1">{createdAt}</span>
            </div>
            <div className="mb-2">
                <strong>Is Completed:</strong>
                <span className="ml-1"><i className={classNames({
                    'fa': true,
                    'fa-check text-success': isCompleted,
                    'fa-times text-danger': !isCompleted
                })}/></span>
            </div>
            <Table striped bordered>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Speed</th>
                    <th>Strength</th>
                    <th>Endurance</th>
                    <th>Distance</th>
                    <th>Time</th>
                </tr>
                </thead>
                <tbody>
                {horses.map(renderHorse)}
                </tbody>
            </Table>
        </Fragment>
    }

    return <Fragment>
        <Row className="mt-4 mb-2">
            <Col md="auto">
                <h3>Game Detail</h3>
            </Col>
            <Col>
                {detail && <Button variant="success" onClick={() => {
                    advance()
                }}>
                    <i className="fa fa-refresh mr-1"/>Advance 10 Seconds
                </Button>}
            </Col>
        </Row>
        {error && <Alert variant="danger">{error}</Alert>}
        {!detail && <Alert variant="warning">Loading</Alert>}
        {detail && renderDetail()}
    </Fragment>
}
