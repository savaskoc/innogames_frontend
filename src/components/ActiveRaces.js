import React, {Fragment, useEffect, useState} from 'react'
import classNames from 'classnames'

import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import Alert from 'react-bootstrap/Alert'
import Table from 'react-bootstrap/Table'

export default function ActiveRaces({api, onSelected}) {
    const [error, setError] = useState(null)
    const [races, setRaces] = useState([])

    const loadRaces = async function () {
        setError(null)
        setRaces([])
        try {
            const {data: {races}} = await api.get('races/active')
            setRaces(races)
        } catch (e) {
            setError(e.message)
        }
    }
    const createRace = async function () {
        setError(null)
        try {
            const {data} = await api.post('races')
            setRaces([...races, {...data, distance: 0, ticks: 0}])
            onSelected(data.id)
        } catch (e) {
            setError(e.message)
        }
    }
    const syncLoadRaces = function () {
        loadRaces()
    }
    useEffect(syncLoadRaces, [])

    const renderRace = function ({id, createdAt, isCompleted, distance, ticks}, i) {
        return <tr key={i}>
            <td>{id}</td>
            <td>{createdAt}</td>
            <td>
                <i className={classNames({
                    'fa': true,
                    'fa-tick text-success': isCompleted,
                    'fa-times text-danger': !isCompleted
                })}/>
            </td>
            <td>{distance}</td>
            <td>{ticks}</td>
            <td>
                <Button size="sm" onClick={() => onSelected(id)}>
                    <i className="fa fa-chevron-right mr-1"/>Details
                </Button>
            </td>
        </tr>
    }

    return <Fragment>
        <Row>
            <Col md="auto">
                <h3>Active Games</h3>
            </Col>
            <Col>
                <Button variant="success" onClick={() => {
                    createRace()
                }}>
                    <i className="fa fa-plus mr-1"/> Create New
                </Button>
                <Button className="ml-2" variant="primary" onClick={syncLoadRaces}>
                    <i className="fa fa-refresh mr-1"/>Refresh
                </Button>
            </Col>
        </Row>
        {error && <Alert variant="danger">{error}</Alert>}
        <Table striped bordered>
            <thead>
            <tr>
                <th>#</th>
                <th>Creation Time</th>
                <th>Is Completed?</th>
                <th>Distance</th>
                <th>Time</th>
                <th className="text-center"><i className="fa fa-cog"/></th>
            </tr>
            </thead>
            <tbody>
            {races.map(renderRace)}
            </tbody>
        </Table>
    </Fragment>
}
