import React, {Fragment, useEffect, useState} from 'react'

import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import Accordion from 'react-bootstrap/Accordion'
import Card from 'react-bootstrap/Card'
import Table from 'react-bootstrap/Table'

export default function LatestRaces({api}) {
    const [races, setRaces] = useState([])
    const fetchRaces = async function () {
        const {data: {races}} = await api.get('stats/latest')
        setRaces(races)
    }
    const syncFetchRaces = function () {
        fetchRaces()
    }

    useEffect(syncFetchRaces, [])

    const renderHorse = function ({id, speed, strength, endurance, distance, ticks}, i) {
        return <tr key={i}>
            <td>{id}</td>
            <td>{speed}</td>
            <td>{strength}</td>
            <td>{endurance}</td>
            <td>{distance}</td>
            <td>{ticks}</td>
        </tr>
    }

    const renderRace = function ({id, createdAt, horses}, i) {
        return <Card key={i}>
            <Card.Header>Race #{id} - {createdAt}</Card.Header>
            <Card.Body>
                <Table striped bordered>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Speed</th>
                        <th>Strength</th>
                        <th>Endurance</th>
                        <th>Distance</th>
                        <th>Time</th>
                    </tr>
                    </thead>
                    <tbody>
                    {horses.map(renderHorse)}
                    </tbody>
                </Table>
            </Card.Body>
        </Card>
    }

    return <Fragment>
        <Row>
            <Col md="auto"><h3>Latest 5 Games</h3></Col>
            <Col>
                <Button variant="primary" onClick={syncFetchRaces}><i className="fa fa-refresh mr-1"/>Refresh</Button>
            </Col>
        </Row>
        <Accordion>
            {races.map(renderRace)}
        </Accordion>
    </Fragment>
}
