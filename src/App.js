import React, {useState} from 'react'
import axios from 'axios'

import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import LatestRaces from './components/LatestRaces'
import BestHorse from './components/BestHorse'
import ActiveRaces from './components/ActiveRaces'
import RaceDetail from './components/RaceDetail'

const api = axios.create({baseURL: process.env.API_URL})

export default function App() {
    const [raceId, setRaceId] = useState(null)

    return <Container fluid>
        <Row>
            <Col md={4}>
                <BestHorse api={api}/>
                <LatestRaces api={api}/>
            </Col>
            <Col>
                <ActiveRaces api={api} onSelected={setRaceId}/>
                {raceId && <RaceDetail api={api} raceId={raceId}/>}
            </Col>
        </Row>
    </Container>
}
